# theme-cylegis.v1/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    theme-cylegis.v1/sass/etc
    theme-cylegis.v1/sass/src
    theme-cylegis.v1/sass/var
