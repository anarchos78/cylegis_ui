Ext.define("cylegis.view.search.SearchForm", {
    extend: "Ext.form.Panel",

    requires: [
        "cylegis.view.search.SearchFormController",
        "cylegis.view.search.SearchFormModel"
    ],

    xtype: "searchform",

    controller: "search-searchform",
    viewModel: {
        type: "search-searchform"
    },

    //bodyPadding: 5,
    width: '100%',
    border: true,

    // The form will submit an AJAX request to this URL when submitted
    url: '', // TODO

    // Fields will be arranged vertically, stretched to full width
    //layout: 'anchor',
    //defaults: {
    //    anchor: '100%'
    //},
    layout: {
        type: 'hbox',
        vertical: false,
        align: 'end'
    },


        // The fields
    //defaultType: 'textfield',
    items: [{
        xtype: 'tbspacer',
        flex: 3
    }, {
        xtype: 'textfield',
        emptyText: 'Παρακαλώ βάλτε όρο αναζήτησης',
        name: 'qs',
        flex: 2
    }, {
        xtype: 'buttongroup',
        //frame: true,
        ui: 'mota',
        columns: 2,
        flex: 0.5,
        items: [{
            glyph: 'xf002@FontAwesome'
        }, {
            text: 'poutsa'
        }]
    }]

    // Reset and Submit buttons
    //buttons: [{
    //    text: 'Reset',
    //    handler: function() {
    //        this.up('form').getForm().reset();
    //    }
    //}, {
    //    text: 'Submit',
    //    glyph:'xf002@FontAwesome',
    //    formBind: true, //only enabled once the form is valid
    //    disabled: true,
    //    handler: function() {
    //        var form = this.up('form').getForm();
    //        if (form.isValid()) {
    //            form.submit({
    //                success: function(form, action) {
    //                    Ext.Msg.alert('Success', action.result.msg);
    //                },
    //                failure: function(form, action) {
    //                    Ext.Msg.alert('Failed', action.result.msg);
    //                }
    //            });
    //        }
    //    }
    //}]
});