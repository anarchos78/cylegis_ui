Ext.define('StoredQueries', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'historyId',
        mapping: 'historyId'
    }, {
        //Used for displaying a tooltip with the whole search term if term length > 50
        name: 'term',
        mapping: 'term'
    }, {
        //Used for displaying the search term.
        //If search term length > 50 then displays a truncated version of the search term
        name: 'trimstr',
        mapping: 'trimstr'
    }, {
        name: 'searchfilter',
        mapping: 'searchfilter'
    }]
});