Ext.define('RecentApofasiSet', {
    extend: 'Ext.data.Model',

    fields: [
        { name: 'apofasi_month', type: 'int' },
        { name: 'apofasi_year', type: 'int' },
        { name: 'apofasi_meros', type: 'int' },
        { name: 'case_num', type: 'auto' },
        { name: 'apofasi_title', type: 'auto' },
        { name: 'ida', type: 'int' },
        { name: 'model', type: 'auto' },
        { name: 'model', type: 'auto' },
        { name: 'type', type: 'auto' },
        { name: 'url', type: 'auto' }
    ],

    belongsTo: 'RecentApofasiDikastirio'
});