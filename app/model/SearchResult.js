
/*
EXAMPLE DATA
[{
    "apofasi_month": 1,
    "apofasi_year": 2015,
    "case_num": "6023/10",
    "content": "Θέμη Θεμιστοκλέους ν. Γεωργίας Φουρνίδου, Αρ.Υπόθεσης: 6023/10, 16/1/2014 \r\n\r\nΕΠΑΡΧΙΑΚΟ ΔΙΚΑΣΤΗΡΙΟ ΠΑΦΟΥ\r\nΕνώπιον: Μ. Αγιομαμίτη, Ε.Δ.\r\n \r\nΑρ.Υπόθεσης:  6023/10 \r\n \r\nΜεταξύ\r\nΘέμη Θεμιστοκλέους\r\n \r\n-ν-\r\n \r\nΓεωργίας Φουρνίδου\r\n                                                                      Κατηγορούμενης\r\n \r\n \r\nΗμερομηνία: 16 Ιανουαρίου, 2014\r\n \r\n \r\nΓια Παραπονούμενο: κ Ζανούππας\r\nΓια Κατηγορούμενη: κα Πρωτοπαπά\r\nΚατηγορούμενη παρούσα\r\n \r\n \r\nΑ Π Ο Φ Α Σ Η\r\n \r\nΗ Κατηγορούμενη αντιμετωπίζει την <span class=\"nom\">κατηγορία</span> της <span class=\"nom\">πρόκλησης</span> μη <span class=\"nom\">εξόφλησης</span> <span class=\"nom\">επιταγής</span> χωρίς <span class=\"nom\">εύλογη</span> <span class=\"nom\">αιτία</span>, κατά παράβαση των  άρθρων 20, 29 και 305 (Α) (2) του Ποινικού Κώδικα Κεφ. 154, ως έχει μέχρι σήμερα τροποποιηθεί.\r\n \r\nΣύμφωνα με τις λεπτομέρειες του κατηγορητηρίου, η Κατηγορούμενη κατά ή περί την 14.04.2010 και/ή πρωτύτερα",
    "doc_id": "6_eparxiaka_poinikes_apofaseis_dikastikes",
    "grid_title": "Θέμη Θεμιστοκλέους ν. Γεωργίας Φουρνίδου, Αρ.Υπόθεσης: 6023/10, 16/1/2014",
    "ida": 6,
    "kwlist": ["εύλογη", "εξόφλησης", "επιταγής", "κατηγορία", "αιτία", "πρόκλησης"],
    "model": "eparxiaka_poinikes_apofaseis_dikastikes",
    "title": "Θέμη Θεμιστοκλέους ν. Γεωργίας Φουρνίδου, Αρ.Υπόθεσης: 6023/10, 16/1/2014",
    "type": "text"
}]
*/

Ext.define('SearchResult', {
    extend: 'Ext.data.Model',

    fields: [
        { name: 'apofasi_month', type: 'int' },
        { name: 'apofasi_year', type: 'int' },
        { name: 'apofasi_meros', type: 'int' },
        { name: 'case_num', type: 'auto' },
        { name: 'content', type: 'auto' },
        { name: 'doc_id', type: 'auto' },
        { name: 'grid_title', type: 'auto' },
        { name: 'ida', type: 'int' },
        { name: 'kwlist', type: 'auto' },
        { name: 'model', type: 'auto' },
        { name: 'title', type: 'auto' },
        { name: 'type', type: 'auto' }
    ]
});
