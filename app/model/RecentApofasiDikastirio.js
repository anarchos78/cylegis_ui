Ext.define('RecentApofasiDikastirio', {
    extend: 'Ext.data.Model',

    fields: [
        { name: 'dikastirio', type: 'auto' }
    ],

    hasMany:{model:'RecentApofasiSet', name:'apofaseis'}

    //associations: {
    //    type: 'hasMany',
    //    model: 'RecentApofasiSet',
    //    name: 'apofaseis',
    //    associationKey: 'apofaseis'
    //}
});
