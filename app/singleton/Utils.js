/**
 * Utility functions
 */

Ext.define('cylegis.singleton.Utils', {
    singleton: true,

    // Development
    //ApofasiUrl: 'http://127.0.0.1/api/v1/apofaseis/',
    //RecentApofaseisUrl: 'http://127.0.0.1/api/v1/recentapofaseis/',
    //SearchResultUrl: 'http://127.0.0.1/api/v1/search/?',
    //StoredQueriesUrl: 'http://127.0.0.1/api/v1/history/',
    //LogoutUrl: 'http://127.0.0.1/amember/login/logout',

    // Production
    ApofasiUrl: 'https://cylegis.com/api/v1/apofaseis/',
    RecentApofaseisUrl: 'https://cylegis.com/api/v1/recentapofaseis/',
    SearchResultUrl: 'https://cylegis.com/api/v1/search/?',
    StoredQueriesUrl: 'https://cylegis.com/api/v1/history/',
    LogoutUrl: 'https://cylegis.com/private/logout',

    csrftoken: function () {
        var csrftoken = Ext.util.Cookies.get('csrftoken');
        return csrftoken
    },

    user_logged_in: function () {
        var user_cookie = Ext.util.Cookies.get('user');
        if (!user_cookie) {
            Ext.Msg.show({
                title: 'Προσοχη!',
                msg: '<span style="display:block!important;white-space:nowrap!important;">' +
                'Δεν είστε συνδεδεμένοι.' +
                '<br>' +
                'Παρακάλω κάντε είσοδο στο λογαριασμό σας.' +
                '</span>',
                buttons: Ext.Msg.OK,
                closable: false,
                fn: function(buttonId) {
                    if (buttonId === "ok") {
                        window.location.replace(cylegis.singleton.Utils.LogoutUrl);
                    }
                }
            });
        }
    },

    check_initial_store_loading: function() {
        var recentapofaseis = Ext.data.StoreManager.lookup('recentapofaseis');
        var storedqueries = Ext.data.StoreManager.lookup('storedqueries');
        var stores = [recentapofaseis, storedqueries];
        var store_flag = 0;

        Ext.each(stores, function(store) {
            store.on('load', function (store, records, successful, eOpts ){});
            store_flag += 1;
        });

        if (store_flag == stores.length) {
            setTimeout(function () {
                Ext.get('loading').remove();
                Ext.get('loading-mask').fadeOut({remove: true, duration: 2});
            }, 1);
        }
    },

    trimtitle: function (title) {
        return title.substr(0, 10) + '...'
    },

    downloadcookie: function (url) {
        var viewport, downloadcookie, cookiePattern, cookieTimer;

        viewport = Ext.ComponentQuery.query('app-main')[0];
        downloadcookie = (new Date()).getTime();
        location.href =  url + '?downloadcookie='  + downloadcookie;
        cookiePattern = new RegExp( ("downloadcookie=" + downloadcookie ), "i");
        cookieTimer = setInterval(checkCookies, 500);

        function checkCookies() {
            if (document.cookie.search(cookiePattern) >= 0) {
                clearInterval(cookieTimer);
                if (viewport) {
                    viewport.setLoading(false);
                }
                //return(console.log("Download complete!!"));
            }
            //console.log("File still downloading...", new Date().getTime());
        }
    },

    returncurrentyear: function () {
        var today = new Date();

        return Ext.Date.format(today, 'Y');
    },

    returncurrentmonth: function () {
        Ext.Date.monthNames = [
            'Ιανουαρίου',
            'Φεβρουαρίου',
            'Μαρτίου',
            'Απριλίου',
            'Μαΐου',
            'Ιουνίου',
            'Ιουλίου',
            'Αυγούστου',
            'Σεπτεμβρίου',
            'Οκτωβρίου',
            'Νοεμβρίου',
            'Δεκεμβρίου'
        ];

        var today = new Date();

        return Ext.Date.format(today, 'F');
    }

});
