Ext.define('cylegis.store.StoredQueries', {
    extend: 'Ext.data.Store',
    model: 'StoredQueries',
    storeId: 'storedqueries',

    autoLoad: true,
    autoSync: true,

    proxy: {
        type: 'ajax',
        api: {
            create: cylegis.singleton.Utils.StoredQueriesUrl,
            read: cylegis.singleton.Utils.StoredQueriesUrl
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json',
            //True to write all fields from the record to the server.
            // If set to false it will only send the fields that were modified.
            // Note that any fields that have "Ext.data.Field.persist" set to false will still be ignored.
            writeAllFields: false,
            rootProperty: 'data'
        },
        headers: {
            'X-CSRFToken': cylegis.singleton.Utils.csrftoken()
        }
    },

    pageParam: false, //to remove param "page"
    startParam: false, //to remove param "start"
    limitParam: false, //to remove param "limit"
    noCache: false, //to remove param "_dc"

    pageSize: 51,

    listeners: {
        load: function (records, operation, success) {
            if (success) {
                //
            } else {
                Ext.Msg.show({
                    title: 'Προσοχη!',
                    msg: '<span style="display:block!important;white-space:nowrap!important;">' +
                    'Δεν είστε συνδεδεμένοι.' +
                    '<br>' +
                    'Παρακάλω κάντε είσοδο στο λογαριασμό σας.' +
                    '</span>',
                    buttons: Ext.Msg.OK,
                    closable: false,
                    fn: function(buttonId) {
                        if (buttonId === "ok") {
                            window.location.replace(cylegis.singleton.Utils.LogoutUrl);
                        }
                    }
                });
            }
        }
    }

});