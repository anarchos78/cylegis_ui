Ext.define('cylegis.store.SearchResult', {
    extend: 'Ext.data.Store',
    model: 'SearchResult',
    storeId: 'searchresultstore',

    fields: [
        'apofasi_month',
        'apofasi_year',
        'apofasi_meros',
        'case_num',
        'content',
        'doc_id',
        'grid_title',
        'ida',
        'kwlist',
        'model',
        'title',
        'type'
    ],

    //autoLoad: true,

    proxy: {
        type: 'ajax',
        pageParam: false, //to remove param "page"
        startParam: false, //to remove param "start"
        limitParam: false, //to remove param "limit"
        noCache: false, //to remove param "_dc"
        //useDefaultXhrHeader: false,
        //url: '/dummy_data/data.json',
        url: cylegis.singleton.Utils.SearchResultUrl,
        extraParams: {},
        reader: {
            type: 'json',
            rootProperty: '',
            successProperty: 'success'
        }
    },

    listeners: {
        load: {
            fn: function(store, records, operation, success) {
                if (success) {
                    var searchresult = Ext.ComponentQuery.query('#searchresult')[0];
                    var searchresulttoolbar = Ext.ComponentQuery.query('#searchresulttoolbar')[0];
                    var apofaseiscounttext = Ext.ComponentQuery.query('#apofaseiscounttext')[0];
                    var apofaseisnum = store.count();
                    var years_from_search_result = this.collect('apofasi_year', false, true).sort();

                    var filterby = Ext.ComponentQuery.query('#filterby')[0];
                    var filterbtn = searchresulttoolbar.query('#filterbtn')[0];
                    var filtermenu = searchresulttoolbar.query('#filtermenu')[0];

                    var html = '<span class="grid-results-count">' +
                        'Συνολικός αριθμός αποφάσεων:&nbsp;' +
                        '</span>' +
                        '<span class="grid-results-num">' +
                        apofaseisnum +
                        '</span>';

                    apofaseiscounttext.setText(html);

                    var filter_obj = [{
                        xtype: 'tbtext',
                        id: 'filterby',
                        html: 'Φιλτράρισμα κατά:'
                    }, {
                        xtype: 'button',
                        id: 'filterbtn',
                        text: 'Χρονολογία',
                        arrowAlign: 'right',
                        menu: {
                            xtype: 'menu',
                            id: 'filtermenu',
                            listeners: {
                                mouseleave: function(menu, e, eOpts) {
                                    Ext.defer(function() {
                                        menu.hide();
                                    }, 1000);
                                }
                            }
                        }
                    }];

                    if (years_from_search_result.length > 1) {
                        if (!filterby) {
                            searchresulttoolbar.add(filter_obj);
                        } else {
                            searchresulttoolbar.remove(filterby, true);
                            searchresulttoolbar.remove(filterbtn, true);
                            searchresulttoolbar.remove(filtermenu, true);
                            searchresulttoolbar.add(filter_obj);
                        }

                        var filter_array = [];
                        var initial_tmp_filter = [];

                        Ext.Array.each(filter_array, function(val) {
                            initial_tmp_filter.push('value == ' + val);
                        });
                        var initial_filter = '(' + initial_tmp_filter.join(' || ') + ')';

                        Ext.Array.each(years_from_search_result, function(year) {
                            searchresulttoolbar.query('#filtermenu')[0].add({
                                xtype: 'menucheckitem',
                                text: year,
                                listeners: {
                                    checkchange: function(item, checked, eOpts) {
                                        var checked_value = item.text;

                                        store.clearFilter();

                                        if (checked) {
                                            Ext.Array.push(filter_array, checked_value);
                                        } else {
                                            Ext.Array.remove(filter_array, checked_value);
                                        }

                                        var tmp_filter = [];
                                        Ext.Array.each(filter_array, function(val) {
                                            tmp_filter.push('value == ' + val);
                                        });

                                        var filter_str = '(' + tmp_filter.join(' || ') + ')';

                                        if (filter_str == '()') {
                                            var filter = '';
                                        } else {
                                            filter = '(' + tmp_filter.join(' || ') + ')';
                                        }

                                        store.filter(function(record) {
                                            var value = record.get('apofasi_year');
                                            if (filter === '') {
                                                //store.clearFilter();
                                                return initial_filter;
                                            } else {
                                                return eval(filter);
                                            }
                                        });

                                    }
                                }
                            });
                        });
                    } else {
                        if (filterby) {
                            searchresulttoolbar.remove(filterby, true);
                            searchresulttoolbar.remove(filterbtn, true);
                            searchresulttoolbar.remove(filtermenu, true);
                        }
                    }
                } else {
                    Ext.Msg.show({
                        title: 'Προσοχη!',
                        msg: '<span style="display:block!important;white-space:nowrap!important;">' +
                        'Δεν είστε συνδεδεμένοι.' +
                        '<br>' +
                        'Παρακάλω κάντε είσοδο στο λογαριασμό σας.' +
                        '</span>',
                        buttons: Ext.Msg.OK,
                        closable: false,
                        fn: function(buttonId) {
                            if (buttonId === "ok") {
                                window.location.replace(cylegis.singleton.Utils.LogoutUrl);
                            }
                        }
                    });
                }
            }
        }
    }

});