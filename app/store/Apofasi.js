Ext.define('cylegis.store.Apofasi', {
    extend: 'Ext.data.Store',
    model: 'Apofasi',
    storeId: 'apofasi',

    fields: [
        'apofasi_month',
        'apofasi_year',
        'apofasi_meros',
        'apofasi_body',
        'apofasi_title',
        'case_num',
        'ida',
        'model',
        'model',
        'type',
        'url'
    ],

    //autoLoad: true,

    proxy: {
        type: 'ajax',
        pageParam: false, //to remove param "page"
        startParam: false, //to remove param "start"
        limitParam: false, //to remove param "limit"
        noCache: false, //to remove param "_dc"
        url: cylegis.singleton.Utils.ApofasiUrl,
        extraParams: {},
        reader: {
            type: 'json',
            rootProperty: '',
            successProperty: 'success'
        }
    },

    listeners: {
        load: function (records, operation, success) {
            if (success) {
                //
            } else {
                Ext.Msg.show({
                    title: 'Προσοχη!',
                    msg: '<span style="display:block!important;white-space:nowrap!important;">' +
                    'Δεν είστε συνδεδεμένοι.' +
                    '<br>' +
                    'Παρακάλω κάντε είσοδο στο λογαριασμό σας.' +
                    '</span>',
                    buttons: Ext.Msg.OK,
                    closable: false,
                    fn: function(buttonId) {
                        if (buttonId === "ok") {
                            window.location.replace(cylegis.singleton.Utils.LogoutUrl);
                        }
                    }
                });
            }
        }
    }

});