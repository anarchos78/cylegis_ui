/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('cylegis.Application', {
    extend: 'Ext.app.Application',
    
    name: 'cylegis',

    launch: function () {
        // TODO - Launch the application
        // Initialize for tooltips
        Ext.tip.QuickTipManager.init();

        // The below block focus to first available tab-panel when TAB key pressed.
        // This is useful to hide the advanced search form fields when using
        // the tab key to navigate the application.
        document.body.addEventListener('keydown', function(e){
            if (e.keyCode == 9) {
                var tab = Ext.ComponentQuery.query('tab')[0];
                tab.focus();
            }
        });

        // Suppress button menu errors
        // [According to WAI-ARIA 1.0 Authoring guide (http://www.w3.org/TR/wai-aria-practices/#menubutton),
        // menu button 'button-xxx' should display the menu on SPACE and ENTER keys, which will conflict with
        // the button handler.]
        Ext.enableAria = false;
        Ext.enableAriaButtons = false;
        Ext.enableAriaPanels = false;
        //Ext.Ajax.disableCaching = false;

        Ext.Ajax.useDefaultXhrHeader = false;
        // Enable CORS support on the XHR object. Currently the only effect of this option is to use the XDomainRequest object instead of XMLHttpRequest if the browser is IE8 or above.
        Ext.Ajax.cors = true;

        // Remove intro loading mask after "recentapofaseis", "storedqueries" got loaded
        cylegis.singleton.Utils.check_initial_store_loading();
        cylegis.singleton.Utils.returncurrentmonth();
    }
});
