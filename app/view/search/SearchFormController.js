Ext.define('cylegis.view.search.SearchFormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.search-searchform',

    // Listen the enter keydown event of searchbox
    onEnterDown: function (field, e) {
        // e.HOME, e.END, e.PAGE_UP, e.PAGE_DOWN,
        // e.TAB, e.ESC, arrow keys: e.LEFT, e.RIGHT, e.UP, e.DOWN
        if (e.getKey() == e.ENTER) {
            // remove focus from "searchbox" to avoid fire
            // "search-btn" click event more than once, using blur method
            // [user my press enter key many times during search results loading]
            var searchbtn = Ext.ComponentQuery.query('#search-btn')[0];
            var searchbox = Ext.ComponentQuery.query('#searchbox')[0];
            searchbox.blur();

            searchbtn.btnEl.dom.click();
        }
    },

    // Listen the onclick event of search button
    onSearchButtonClick: function () {
        //http://cylegis.com/api/v1/search/?q=*:*&fq=model:aad_apofaseis_dikastikes&fq=apofasi_month:10&fq=apofasi_meros:1&fq=apofasi_year:2015
        // OR
        //http://cylegis.com/api/v1/search/?q=*:*&fq=model:(aad_apofaseis_dikastikes OR eparxiaka_ponikes_apofaseis_dikastikes)

        // If user isn't logged in, just redirect him/her to aMember login page
        cylegis.singleton.Utils.user_logged_in();

        var clearbtn = Ext.ComponentQuery.query('#clear-btn')[0];
        var keepfilterscheckbox = Ext.ComponentQuery.query('#keepfilters')[0];
        var searchbox = Ext.ComponentQuery.query('#searchbox')[0];
        var model = Ext.ComponentQuery.query('#model')[0];
        var apofasi_meros = Ext.ComponentQuery.query('#apofasi_meros')[0];
        var apofasi_num = Ext.ComponentQuery.query('#apofasi_num')[0];
        var apofasi_month = Ext.ComponentQuery.query('#apofasi_month')[0];
        var case_num = Ext.ComponentQuery.query('#apofasi_case_num')[0];
        var apofasi_year = Ext.ComponentQuery.query('#apofasi_year')[0];

        var searchbox_val = searchbox.getSubmitValue();
        var model_val = model.getSubmitValue();
        var apofasi_meros_val = apofasi_meros.getSubmitValue();
        var apofasi_num_val = apofasi_num.getSubmitValue();
        var apofasi_month_val = apofasi_month.getSubmitValue();
        var apofasi_year_val = apofasi_year.getSubmitValue();
        var case_num_val = case_num.getSubmitValue();
        var aad_index = model_val.indexOf('aad_apofaseis_dikastikes');
        var storedqueriesstore = Ext.data.StoreManager.lookup('storedqueries');
        var storedqueriescombo = Ext.ComponentQuery.query('#storedqueries')[0];

        // Keep filters checkbox functinality
        if (keepfilterscheckbox.getRawValue()) {
            // empty block [equivalent to Python pass]
            //console.log('model_val :: ', model_val);
            //console.log('apofasi_meros_val :: ', apofasi_meros_val);
        } else {
            clearbtn.btnEl.dom.click();
        }

        // Build the searchterm
        if (!searchbox_val) {
            var q = '*'
        } else {
            q = searchbox_val
        }

        // Build the apofasi_meros filter
        if (!apofasi_meros_val) {
            var apofasi_meros_fq = ''
        } else if (apofasi_meros_val.length == 1) {
            apofasi_meros_fq = 'apofasi_meros:' + apofasi_meros_val[0]
        } else {
            apofasi_meros_fq = 'apofasi_meros:(' + apofasi_meros_val.join(' OR ') + ')'
        }

        if (!apofasi_num_val) {
            var apofasi_num_fq = ''
        } else {
            apofasi_num_fq = 'apofasi_num:(' + apofasi_num_val + ')';
        }

        if (!model_val) {
            var model_fq = ''
        } else if (model_val.length == 1) {
            model_fq = 'model:' + model_val[0]
        } else {
            model_fq = 'model:(' + model_val.join(' OR ') + ')';
            //if (apofasi_meros_val.length > 0) {
            //    model_val.splice(aad_index, 1);
            //    model_fq = 'model:(' + model_val.join(' OR ') + ')'
            //} else {
            //    model_fq = 'model:(' + model_val.join(' OR ') + ')'
            //}
        }

        // Build the apofasi_month filter
        if (!apofasi_month_val) {
            var apofasi_month_fq = ''
        } else if (apofasi_month_val.length == 1) {
            apofasi_month_fq = 'apofasi_month:' + apofasi_month_val[0]
        } else {
            apofasi_month_fq = 'apofasi_month:(' + apofasi_month_val.join(' OR ') + ')'
        }

        if (!case_num_val) {
            var case_num_fq = ''
        } else {
            case_num_fq = 'case_num:(' + case_num_val + ')';
        }

        // Build the querystring after running validation apofasi_year value[s]
        if (apofasi_year.validateValue(apofasi_year_val)) {
            // Build the apofasi_year filter
            if (!apofasi_year_val) {
                var apofasi_year_fq = '';
            } else {
                if (apofasi_year_val.indexOf(',') > -1) {
                    apofasi_year_fq = 'apofasi_year:(' + apofasi_year_val.split(',').join(' OR ') + ')';
                } else if (apofasi_year_val.indexOf('-') > -1) {
                    apofasi_year_fq = 'apofasi_year:[' + apofasi_year_val.split('-').join(' TO ') + ']';
                } else {
                    apofasi_year_fq = 'apofasi_year:' + apofasi_year_val;
                }
            }

            // Build the whole querystring
            // Example :: (model:(eparxiaka_astikes_apofaseis_dikastikes) OR apofasi_meros:(2 OR 4)) AND apofasi_month:(10) AND apofasi_year:(2008 OR 2015)
            var fq_array = [model_fq, apofasi_meros_fq, apofasi_month_fq, apofasi_year_fq, apofasi_num_fq, case_num_fq];

            var solr_fq = [];
            for (var i= 0; i < fq_array.length; i++) {
                if (fq_array[i].length != 0) {
                    solr_fq.push(fq_array[i])
                }
            }

            if (solr_fq.length) {
                var solr_fq_str = '(' + solr_fq.join(' AND ') + ')';
                //if (model_fq.length) {
                //    if (model_fq.indexOf('aad_apofaseis_dikastikes') > -1) {
                //        var solr_fq_str = '(' + solr_fq.join(' AND ') + ')';
                //    } else {
                //        if (solr_fq.length < 3) {
                //            solr_fq_str = '(' + solr_fq.join(' AND ') + ')';
                //        } else {
                //            var model_fq_tmp = solr_fq[0];
                //            var apofasi_meros_tmp = solr_fq[1];
                //            solr_fq.splice(0, 2);
                //
                //            var special_case_head = '(' + model_fq_tmp + ' OR ' + apofasi_meros_tmp + ')';
                //            var special_case_tail = solr_fq.join(' AND ');
                //            solr_fq_str = '(' + special_case_head + ' AND ' + special_case_tail + ')';
                //        }
                //    }
                //} else {
                //    solr_fq_str = '(' + solr_fq.join(' AND ') + ')';
                //}
            } else {
                solr_fq_str = false
            }

            var contenttappanel = Ext.ComponentQuery.query('#content')[0];
            var searchresulttab = Ext.ComponentQuery.query('#searchresult')[0];
            var searchresultstore = Ext.data.StoreManager.lookup('searchresultstore');

            if (searchresulttab) {
                //contenttappanel.setActiveTab(searchresulttab);
                contenttappanel.remove(searchresulttab, false); //remove the tabpanel without destroying it
                contenttappanel.insert(-1, searchresulttab); //after removal "reposition" the tabpanel to the left
                contenttappanel.setActiveTab(searchresulttab); //set tabpanel active

            } else {
                searchresulttab = Ext.create({
                    xtype: 'searchresult'
                });

                contenttappanel.add(searchresulttab);
                contenttappanel.setActiveTab(searchresulttab);
            }

            if (solr_fq_str) {
                searchresultstore.load({params: {q: q, fq: solr_fq_str}});
            } else {
                if (q == "*") {
                    var current_year = cylegis.singleton.Utils.returncurrentyear();
                    var current_year_filter = 'apofasi_year:' + current_year;
                    searchresultstore.load({params: {q: q, fq: current_year_filter}});
                } else {
                    searchresultstore.load({params: {q: q}});
                }
            }

            //insert your search term into "search history items" after search [successful or unsuccessful search]
            if (searchbox_val.length >= 48) {
                var trimedterm = searchbox_val.substr(0, 47) + '...'
            } else if (searchbox_val.length <= 47 && searchbox_val !== '')  {
                trimedterm = searchbox_val
            }

            if (solr_fq_str) {
                var searchfilter = solr_fq_str
            } else {
                searchfilter = 'None'
            }

            var mysearchrerm = [{
                term: searchbox_val,
                trimedterm: trimedterm,
                searchfilter: searchfilter
            }];

            if (searchbox_val.length) {
                storedqueriesstore.insert(0, mysearchrerm);
            }

            //if (searchbox_val.length >= 48) {
            //    var mysearchrerm = [{
            //        term: searchbox_val,
            //        trimedterm: searchbox_val.substr(0, 47) + '...'
            //    }];
            //    storedqueriesstore.insert(0, mysearchrerm);
            //} else if (searchbox_val.length <= 47 && searchbox_val !== '') {
            //    mysearchrerm = [{
            //        term: searchbox_val,
            //        trimedterm: searchbox_val
            //    }];
            //    storedqueriesstore.insert(0, mysearchrerm);
            //}

            // IMPORTANT :: CLEAR FILTERS JUST IN CASE THAT THERE ARE FILTERS APPLIED ON IT
            searchresultstore.clearFilter();

        } else {
            var errmsg = 'Μη έγκυρα στοιχεία!<br><br>' +
                         'Παραδείγματα:<br>' +
                         'Για εύρος ετών: 2012-2015<br>' +
                         'Για μεμονωμένα έτη: 2000,2005,2015';
            Ext.Msg.alert('Προσοχή', errmsg);
        }
    },

    // Listen the toggle event of search button
    onAdvanceSearchButtonToggle: function () {
        var form = Ext.ComponentQuery.query('#searchform')[0];
        var advsearchbtn = Ext.ComponentQuery.query('#adv-search-btn')[0];
        var searchbox = Ext.ComponentQuery.query('#searchbox')[0];
        var minusglyph = 'xf068@FontAwesome';
        var plusglyph = 'xf067@FontAwesome';

        var searchboxheight = searchbox.getHeight();

        if (advsearchbtn.pressed == true) {
            advsearchbtn.setGlyph(minusglyph);
            form.setHeight(searchboxheight * 3 + 42);
        } else {
            advsearchbtn.setGlyph(plusglyph);
            form.setHeight(searchboxheight + 22);
        }
    }
    
});
