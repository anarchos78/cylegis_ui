Ext.define('cylegis.view.searchresult.SearchResultController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.searchresult-searchresult',

    onRowSelect: function (rowmodel, record, index, eOpt) {
        var ida = record.data.ida;
        var doc_id = record.data.doc_id; // use it for apofasi_tab id
        var model = record.data.model;
        var apofasi_month = record.data.apofasi_month;
        var apofasi_year = record.data.apofasi_year;
        //var title = record.data.title.replace(/nom/gi, 'nomd');
        var title = record.data.title.replace(/<(?:.|\n)*?>/gm, '');
        var case_num = record.data.case_num;
        var apofasi_meros = record.data.apofasi_meros;
        var apofasi_body = record.data.content;
        var kwlist = record.data.kwlist;
        var type = record.data.type;

        var contenttabpanel = Ext.ComponentQuery.query('#content')[0];

        var existingapofasitab = contenttabpanel.items.findBy(function (i) {
            return i.id == model + '_' + ida;
        });

        // TODO the below snippet is useful to check if tab exists and pull the custom properties  to download docx and/or PDF
        //if (existingapofasitab != null) {
        //    console.log(existingapofasitab.aaaaaa);
        //}

        switch (model) {
            case 'aad_apofaseis_dikastikes':
                var paneltitle = 'Ανώτατου - ';
                break;
            case 'eparxiaka_astikes_apofaseis_dikastikes':
                paneltitle = 'Επ. Αστικές - ';
                break;
            case 'eparxiaka_enoikiaseon_apofaseis_dikastikes':
                paneltitle = 'Επ. Ενοικιάσεων - ';
                break;
            case 'eparxiaka_ergatiko_apofaseis_dikastikes':
                paneltitle = 'Επ. Εργατικό - ';
                break;
            case 'eparxiaka_oikogeneiako_apofaseis_dikastikes':
                paneltitle = 'Επ. Οικογενειακό - ';
                break;
            case 'eparxiaka_poinikes_apofaseis_dikastikes':
                paneltitle = 'Επ. Ποινικές - ';
                break;
            case 'dioikitika_apofaseis_dikastikes':
                paneltitle = 'Διοικητικών - ';
                break;
        }

        if (!existingapofasitab) {
            if (type != 'text') {
                var user = Ext.util.Cookies.get('user');
                if (user == null || user == undefined) {
                    Ext.Msg.show({
                        title: 'Προσοχη!',
                        msg: '<span style="display:block!important;white-space:nowrap!important;">' +
                        'Δεν είστε συνδεδεμένοι.' +
                        '<br>' +
                        'Παρακάλω κάντε είσοδο στο λογαριασμό σας.' +
                        '</span>',
                        buttons: Ext.Msg.OK,
                        closable: false,
                        fn: function(buttonId) {
                            if (buttonId === "ok") {
                                window.location.replace(cylegis.singleton.Utils.LogoutUrl);
                            }
                        }
                    });
                } else {
                    var url = cylegis.singleton.Utils.ApofasiUrl + model + '/' + ida + '/';
                    var apofasitabpdf = Ext.create('cylegis.view.content.PdfViewer', {
                        id: model + '_' + ida,
                        //title: paneltitle + case_num,
                        title: paneltitle + cylegis.singleton.Utils.trimtitle(title),
                        tooltip: title,
                        downloadmodel: model,
                        download_id: ida
                    });

                    apofasitabpdf.down('component').setSource(url);
                    contenttabpanel.add(apofasitabpdf);
                    contenttabpanel.setActiveTab(apofasitabpdf);
                }
            } else {
                var apofasitab = Ext.create('cylegis.view.content.ApofasiTextPanel', {
                    id: model + '_' + ida,
                    //title: paneltitle + case_num,
                    title: paneltitle + cylegis.singleton.Utils.trimtitle(title),
                    tooltip: title,
                    downloadmodel: model,
                    download_id: ida
                });

                var dataview= apofasitab.down('dataview');
                var dataviewstore = Ext.create('cylegis.store.Apofasi');
                var dataviewstoreurl = cylegis.singleton.Utils.ApofasiUrl + model + '/' + ida + '/';
                var cyclebtnid = apofasitab.down('toolbar').items.items[0].id;
                var cyclebtn = Ext.ComponentQuery.query('#' + cyclebtnid)[0];

                dataview.bindStore(dataviewstore);
                dataviewstore.load({
                    scope: this,
                    url: dataviewstoreurl,
                    params: {
                        kwlist: String(kwlist)
                    }
                });

                // Remove the text highlight button if no highlight terms have been found
                if (!kwlist.length) {
                    cyclebtn.destroy();
                }

                contenttabpanel.add(apofasitab);
                contenttabpanel.setActiveTab(apofasitab);
            }
        } else {
            contenttabpanel.setActiveTab(existingapofasitab);
        }

    }
});
