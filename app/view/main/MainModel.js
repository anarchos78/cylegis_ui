/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('cylegis.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        logoName: '<img alt="Cylegis" width="100" height="37" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAAlCAYAAAHD4XwLAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDoyNjEwN0RBODFFQTNFNTExODZEQUZBNkY5RDA3MUI3NyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpDNDY2MTE1QTU5QkUxMUU2QjI3NDk2MUNDRjAzM0VBNyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpDNDY2MTE1OTU5QkUxMUU2QjI3NDk2MUNDRjAzM0VBNyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjY2QTk3MzkyQkU1OUU2MTE5QTI0QkNBRjQyRkNFNTIxIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjI2MTA3REE4MUVBM0U1MTE4NkRBRkE2RjlEMDcxQjc3Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+ydMXngAAC1RJREFUeNpi/A8EDJiAkQEPYIIx7r75iSzuj0VtNJz1Hw207HgKYzJAMT8QHwfi0zBxuE0R8++C6duvfsDNg+KPQPwPxe3k+AkggLBp+g3EbAQDYcbhVwxEAZAN8Yvugn0WPvf2/1eff/+CehwZvARiVbBjYCLRC+7833z5PYj5CypUiKThBVTDf5L9ABBAKE7CA34hxRHJGMV/IXNu/1908g0uS/6TawkTsq+42ZgYYs2E4fy4RfcYIqEJCD0AQKkYiicDcRYQ8wDxSyA+DcSqQKwFVYeZhJFBwuK7xAbXZag6BWzyuFIvA0kRSwAABBCxljAyUACYQufeYUhacp+BpgA9HuYff/1//61P6MICQGxIZGr6hjcJx0Hzy7dff9EtcQPZj8dgRqKS8MwjrxiijCHJl5OViSFi3h14MsYCfgFxLhB7QpOpCjy5QmhQ8l0FxAEowXXt+bf/MQsROf/Fp1//111493/qoZfoPrEA4tdADFI8BymTotNfYWyU4Mpa+QBskf/MW//Rihs3JGXIeriAeDsWuTAo/RnDEmQQCixicoCWElGcgCL6AD519MknDHQAAAEE9knfvhcMx+99YUiyFGXw1OYn1yyKih5KATi0lITZGdhYGBnkhQbMHdSJEWwSf/79Z4hecBec93/8gSTEv8AWzpoUFXwx4gPEO4F4ARAnUsmN54HYAIjVQE0vXIpYsAleevqNAZTcpAXYGPqC5FDkVp17x7D+4nsGAxkuhnJXSYaLQLUdu54zqIpyMDT5SONzEEjyCRL/ItSBIFADxM1Q9gsgloA2DgVAYYqWUW8AsTp6BsaaEU/c/wKmrZV4MOTCjIQY5IBJ8PKzb+CithPoCWDkEfKEOtQToJrxCBAfBmJ9IN4BxK1QT5RDHaUK1fMTS8OFD8kT+4GYlWDSArWCOID5homJkSHPQZxBkIuF4fTDrwzBBoJweVZmRqAn/jPMiVYCqUVOWgeAeBrUKJBly6AO+QctEP6CGl9A/BVa7VyFqgXVZSuA2BWIQY1lcWjrygSatB4CMTMQf0dKcqDS6RNR5e/7b38ZGIFhJcDJDBdr3fGM4dbrHwwVwOSlKcGJXGrxYym7PyDxDaBJ+goQ/8BinTEQn4EGhiMQc0Ed/xUaECLQWLsFxG9xNlOIAeHz7vzv2/ucmq3x/UjmvAViPVLNILZGHPT1CEAAUdMjKtAexMBUiKASJ2TOHWA78S7DUAZMB259YuBhZwLX7K8+/xm6HnFS5wOWSIzg4lWMl2X4NVFAAJTsdl7/yHDvzQ+wRx1V+Rgk+VlxKVeAlveg4nYNFd34BohnAXEVWR6ZffQ1wz5gsmMBVnq//vwDxxqops+1F8dllgS0eUFxux+tUfsXWt9w4lOINS0Bu7sMR+99BleC08MVwHkIBH78/keVVIDUoSQE/hEbKFjbWrOAsQFq/c6JUoR7AgQ4gC1hv5m3GQJn34GLgUq8ICQ+HnAYqZkCouXQ2lEwvBJKSyDJ/YayVdHU+uH1CKj99PvvfwYuNkxpF2DhwM/BBG9YgtpbwtzMhDwBahDaQNtisBB+CKUfQOn50DwWBuX/QPMoCFxHilVFIN5GsBuKK+6zbMXAnlx57i3DlWffGdiBxXa8uQghj8Ca6CXQmPmMJCcPpZOA+CwQP8PjhEdI4x8WSE187B5hAbZ4QR0qkEMx5IAxwAmMqWcffzPsvfmR4Tsw39go8xLyCCtSn0QKiF9Dxw1JCUNwZxY6QAMybzlSCxu7R4qdJRlAZVn7rmcMC0++YXj8/hew//EdWAxDugjZduJgj557/A3c+cICQEkoFIq1gXgvVHwKECtDsQFS6IJAIzS54OvYgJJnOFLyDCRY/B6++5lh0oGXwKTDBC1m/jMYy3IzFDpB8iBo6Ask1+wrwyAnyIZe/CKDdmgdgG7PA6jDedCS2kNochOA9hL/IzVI0c2wAuLjOItfELAFJhcQ/gnsr3/+8ZeBD9gXYWOGBARIDNTh+gsMA6gnGKAOEsRi2U+kDCoExBpA/BzaWwSBL1A5WLH8Fyr+EUrzI5nJCO1ZgugLFLV+QX32DZfeg5PelDB5YInFQmnrVxE6qLAHiC2h3dm30A4U7QbPLBV5GJiBNeWEYDlkT1ACJKGx4g71xCpSPTG8+iMMwwQMG48ABGjf6mKbKsPwy0pZN7t1ZXZTw7KZEMLQjQAKjAVuBCQQJuIFEAj4swhEE++4AKPGhJAwb7gaQWM0kQsSvJAtgRlCWGATWED+Efkbsjjpur/SdlvXFd/nO+9Zzw7Q9cfSJu5N3pyc9nxfz77ne/+e99vo1gpytL7wwE8eX4imu2w0s9iWrneCqy1P5zZNpyivc6kjQLub/h7NEZFKFXAdVbem5Kl544Sk2NTtNgtZOE6HpGoa5krEzkAgAZmQNDIPXeyuvAMj9JLDqnr7Ey4rZnFIXeFJKRUUTc7d91PjlV662z1EQc6EQVJMMhTSIXZ7iEV7aqYp64sTkBLJcJsMn//I+n6GAQEQ0FGYIfcj8t7Xkooh8Uj9KTcdv6kxrJMk3rxot9KMIhu57Np03f4Q3Xg4QG+XO+IFI5o8zkDLwOapZa1jRan/TTJgxAUIrGBnwwPq7B+mPAYDNN2a2U5aP68w6rgfznio0xtUVAZqymUz82lBWaQr6eW68/vfutR3sDJ+LOuDha7JXOUkAwAK1vdAuAkzMFnq0QYhnB49q9Bi3S673CK7/2uQecL9AIAA6y7WHhmDftqfwuUsEnLqocmdbWFdKZTLYyEDzrJ+Z+CE4gOk8Wof/dUTVFkXuB/s/vHAgOAIxa6GDgUopKMvyNaUQ85cjc1rut5PrXd9at4Az1tb5QozGCFKjDhGoxFt3vkGoqBNXAsWbinrPqFzlhrGfSSLM4aglFi2zvS5RwDRZbUwirrF7DUAckJ+V5c7AugSIeTqEy6osJBG71M2NTumccV5VvpkcbGKKRjfGwjRofNaE7afEwgw/dlyqmJBqZ2WlzuSsfZzBjAOys5eLH98PkXo2bcEGMhaExhnxDp0nWNgdHSgRmJ4n/kmMC4JeJWkdRQcYrWJAYJDDmFxIlnsW862+2JeqTklubRu3lQKBMOKmATnd88zRM23H6l4AyMuZIuprXYl434XCpmpy0YaS9j7KUIvQ+bKdaVpnn009jjxractXCx5D+sqsQgSN3jd8D7H6MmjCLEDUlPhVIEbvh68+9XOAfrs8H1q7wnGNP6dSqeiuBF7YCn7T3Ny8Ee/6rSgA/NhVRE5bJZkgnq7sGK6HBa3Z5GYki1Xfefru/d30zybTPdlpB0gSUQQT16Q90D8+IIiHR6wdxfJ1H+LO+09crmPfmrzqB62VZG+IIDDymoAlE7DgCT+uLqIVlcUjI7Fwn/O8QSUvlXIY1jNu5wcbHijcLy0N5ro7PgK1qOm7+Cmbkg8ASFdwdoi7kOX/axbTeN84gbNHJJb0lydkG4zxZDXxKpKJeNC3+SkBO9Z4iJ1Q/hVgEk87a2pLFCKBW+956OLHQH6xzusFhZtOqS5ztwsev3lHMo37XhkWhvfLFSH0yBIDha9ajeCMbpRSGu3eUk7xhRt0+QbsqZjMha9kx1yLTW4qqC4HzdFjlZBtpF26ncz6wbS+jKY8xeJLy2SrelADZqCfJ/MPUiRzhnmuCAbwFg/4dlm1i8lrvw3hWE8gthzu2uQDrZ10033IOWwJfkZwCXT8+jTJ1v16ajUpwgYrWJN+prAUtHVqzY8+xVpDbXUkYuplhNcSB5ocau2aZjRsVkttHPFK+oIZ4YIUttvx3lmSBKFn58bl5UqQUzBiYJSTpVjOBWdTi7LJe7NKfco/vCfDb1pIRcziI743/ZDJvj1DJN/AbP0Xfox3jmDAAAAAElFTkSuQmCC">',
        instructions: '<span style=\"margin:0 auto;display:table;color:#3a3a3a;font-weight:bold;font-size:16px;\">' +
        'Καλωσορίσατε στη διαδικτυακή εφαρμογή νομικής πληροφόρησης "CyLegis  Νομολογία".' +
        '</span>' +

        '<br><br><br>' +

        '<span style=\"color:#181e2b;font-size:14px;line-height:20px;\">' +
        '<span style=\"margin:0 auto;display:table;color:#3a3a3a;font-weight:bold;font-size:14px;\">ΟΔΗΓΙΕΣ ΧΡΗΣΗΣ</span>' +

        '<br><br>' +

        '<span style="margin:0px auto;display:table;">' +
        '<img src="resources/instructions/img1.png" alt="img1" width="750" height="421">' +
        '</span>' +

        '<br><br>' +

        '1. Επιλέγοντας <b>«Οδηγίες»</b> ανοίγει η καρτέλα με τις οδηγίες χρήσης του <b>"CyLegis Νομολογία"</b>.' +
        '<br><br>' +
        '2. Επιλέγοντας <b>«Αναζήτηση»</b> ανοίγει η καρτέλα αναζήτησης.' +
        '<br><br>' +
        '3. Το ιστορικό αναζήτησης: Στη μπάρα αυτή αποθηκεύονται αυτόματα όλες οι αναζητήσεις, σε μορφή λίστας, στις οποίες μπορείτε να ανατρέξετε αργότερα.' +
        '<br><br>' +
        '4. Η μπάρα αναζήτησης: Εδώ πληκτρολογείτε τις αναζητήσεις που επιθυμείτε να πραγματοποιήσετε.' +
        '<br><br>' +
        '5. Πλήκτρο αναζήτησης (συμβολίζεται με τον μεγεθυντικό φακό): Επιλέγοντάς το εκτελείται η αναζήτηση που έχετε πληκτρολογήσει. Πλήκτρο σύνθετης αναζήτησης (συμβολίζεται με το <b>«+»</b>): Επιλέγοντάς το αναπτύσσεται η σύνθετη αναζήτηση.' +
        '<br><br>' +
        '6. Παράθεση πρόσφατων αποφάσεων: Λίστα με τις δικαστικές αποφάσεις που εισήχθησαν τον τελευταίο μήνα στη βάση δεδομένων του <b>"CyLegis Νομολογία"</b>. Κατηγοριοποιημένες ανά δικαστήριο.' +

        '<br><br>' +
        '<br><br>' +

        '<span style="margin:0px auto;display:table;">' +
        '<img src="resources/instructions/img2.png" alt="img2" width="750" height="421">' +
        '</span>' +

        '<br><br>' +

        '7. Σύνθετη αναζήτηση: Επιλέγοντάς το σχετικό πλήκτρο αναπτύσσεται το μενού της σύνθετης αναζήτησης. Από αριστερά προς δεξιά έχουμε κατα σειρά: ' +

        '<br><br>' +

        '&nbsp;&nbsp;&nbsp;&nbsp;α. Διατήρηση φίλτρων: Όσο είναι επιλεγμένο διατηρούνται τα φίλτρα ενεργά σε οποιάδηποτε αναζήτηση πραγματοποιείτε. Αποεπιλέγοντάς το καθαρίζονται τα φίλτρα.'+
        '<br><br>' +
        '&nbsp;&nbsp;&nbsp;&nbsp;β. Φίλτρο <b>«Αποφάσεις»</b>: Επιλέγετε σε ποια δικαστήρια επιθυμείτε να αναζητήσετε δικαστικές αποφάσεις. Μπορεί να είναι ένα ή και περισσότερα.'+
        '<br><br>' +
        '&nbsp;&nbsp;&nbsp;&nbsp;γ. Φίλτρο <b>«Μέρος»</b>: Επιλέγετε το μέρος του Ανωτάτου Δικαστηρίου.'+
        '<br><br>' +
        '&nbsp;&nbsp;&nbsp;&nbsp;δ. Φίλτρο <b>«Αριθμός απόφασης»</b>: Πληκτρολογείτε τον αριθμό απόφασης που επιθυμείτε να εντοπίσετε.'+
        '<br><br>' +
        '&nbsp;&nbsp;&nbsp;&nbsp;ε. Φίλτρα <b>«Μήνας»</b>, <b>«Αριθμός Υπόθεσης»</b> και <b>«Έτος»</b>: Τα φίλτρα αυτά σας δίνουν τη δυνατότητα να αναζητήσετε αποφάσεις ανά μήνα και έτος δημοσίευσης της απόφασης, αλλά και αριθμό υπόθεσης, αγωγής κλπ.<br>' +
        'Στο φίλτρο <b>«Έτος»</b> μπορείτε να ορίσετε περισσότερα από ένα έτη.<br>' +
        'Έτσι, για να αναζητήσουμε αποφάσεις μεταξύ των ετών 2000 και 2004, στο φίλτρο <b>«Έτος»</b> γράφουμε: <b>2000-2004</b><br>' +
        'Αν θέλουμε να αναζητήσουμε μεμονωμένα έτη π.χ. 1999, 2001, 2010 και 2016, τότε στο φίλτρο <b>«Έτος»</b> γράφουμε: <b>1999,2001,2010,2016</b> <i>(Σημ. μπορείτε να  θέσετε όσα έτη θέλετε με οποιαδήποτε σειρά)</i><br>' +
        'Σημειώνουμε ότι με το φίλτρο <b>«Μήνας»</b> μπορείτε πολύ εύκολα να αναζητήσετε αποφάσεις περασμένων μηνών. Μπορείτε κάλλιστα να το χρησιμοποιείτε για να εντοπίζετε τις αποφάσεις του προηγούμενου μήνα.'+
        '<br><br>' +

        '8. Φιλτράρισμα κατά χρονολογία: Με το φίλτρο αυτό περιορίζετε τα αποτελέσματα που έχουν ήδη επιστραφεί από τη μηχανή αναζήτησης στα έτη που επιθυμείτε μόνο.' +
        '<br><br>' +
        '9. Καρτέλα <b>«Αποτελέσματα αναζήτησης»</b>: Εδώ εμφανίζονται τα αποτελέσματα των αναζητήσεών σας. Επιλέγοντας μία απόφαση ανοίγει αυτόματα νέα καρτέλα.'+

        '<br><br>' +
        '<br><br>' +

        '<span style="margin:0px auto;display:table;">' +
        '<img src="resources/instructions/img3.png" alt="img3" width="750" height="421">' +
        '</span>' +

        '<br><br>' +

        '10. Πλήκτρο <b>«Υπογράμμιση ενεργή/ανενεργή»</b>: Με το πλήκτρο αυτό ενεργοποιούμε και απενεργοποιούμε από το κείμενο της απόφασης την υπογράμμιση των λημμάτων που έχουμε αναζητήσει.'+
        '<br><br>' +
        '11. Πλήκτρο <b>«Αποθήκευση»</b>: Με το πλήκτρο αυτό αποθηκεύουμε σε μορφή word ή pdf την απόφαση που έχουμε προβάλλει.'+
        '<br><br>' +
        '12. Το πλήρες κείμενο της απόφασης.'+

        '<br><br>' +

        '<span style="margin:0px auto;font-family:verdana;font-size:15px;font-weight:bold;display:table;">* * *</span>' +

        '<br>' +

        '<span style=\"margin:0 auto;display:table;color:#3a3a3a;font-weight:bold;font-size:14px;\">ΟΔΗΓΙΕΣ ΑΝΑΖΗΤΗΣΗΣ</span>' +
        '<br>' +
        '<b>Απλή αναζήτηση</b>' +
        '<br>' +
        'Χρησιμοποιήστε την <b>μπάρα αναζήτησης</b>, που βρίσκεται στο επάνω δεξί μέρος της εφαρμογής για να εισάγετε τους όρους που επιθυμείτε να αναζητήσετε. Επί παραδείγματι εισάγοντας τον όρο <b>«<i>κληρονομιά</i>»</b> και πατώντας το <i>πλήκτρο</i> <b>αναζήτησης</b> εμφανίζονται οι αποφάσεις  που περιέχουν τον όρο <i>κληρονομιά</i> και τα παράγωγά του (π.χ. κληρονόμος, κληρονομικά, κληρονομήθηκε κ.λπ.). Εάν συνδυάσουμε τους όρους <b>«<i>αποποίηση</i>»</b> και <b>«<i>κληρονομίας</i>»</b> θα εμφανιστούν μόνο οι αποφάσεις που περιέχουν αυτούς τους όρους (και τα παράγωγα τους φυσικά). Εάν επιθυμείτε να αναζητήσετε συγκεκριμένες φράσεις πρέπει να τις περικλείσετε με εισαγωγικά (\"\") . Έτσι εισάγοντας τους όρους <b>\"<i>αποποίηση κληρονομίας</i>\"</b> εντός εισαγωγικών θα εμφανιστούν οι αποφάσεις που περιέχουν τη φράση <b>«<i>αποποίηση κληρονομίας</i>»</b>, <b>«<i>αποποιήσης κληρονομίας</i>»</b> κ.λ.π. και όχι οι φράσεις <b>«<i>αποποίηση της κληρονομίας</i>»</b> κ.λ.π..' +
        '<br>' +
        '' +
        '<br><br>' +
        'Η μηχανή αναζήτησης της εφαρμογής <b>ΔΕΝ</b> σας υποχρεώνει:' +
        '<br>' +
        '<ul>' +
        '<span style="margin-left:14px;display:table;">' +
        '<li>να χρησιμοποιείτε τόνους στους όρους που εισάγετε</li>' +
        '<li>να χρησιμοποιείτε αστερίσκους</li>' +
        '<li>να πληκτρολογείτε επακριβώς τους όρους που επιθυμείτε, αρκεί να σέβεστε τους κανόνες ορθογραφίας</li>' +
        '</span>' +
        '</ul>' +
        '<b>Για αναζήτηση νόμων και άρθρων τους</b> κρίνεται απαραίτητο –αλλά όχι υποχρεωτικό– να περικλείετε τον νόμο εντός εισαγωγικών και απέξω από αυτά να αναγράφετε τα άρθρα που επιθυμείτε να αναζητήσετε. <br>Π.χ.: <b>\"94(1)/1994\" άρθρο 2</b><br>Για <b>ακόμη καλύτερα αποτελέσματα</b> σας συνιστούμε ανεπιφύλακτα να χρησιμοποιείτε την <b>αναζήτηση <b>«εύρους»</b></b> την οποία επεξηγούμε στην τελευταία ενότητα με τίτλο <b><b>«Προηγμένες αναζητήσεις με χρήση ειδικών χαρακτήρων»</b></b>.' +
        '<br><br>' +

        '<span style="margin:0px auto;font-family:verdana;font-size:15px;font-weight:bold;display:table;">* * *</span>' +

        '<br>' +

        '<b>Χρήση φίλτρων</b>' +
        '<br>' +
        'Εφόσον πραγματοποιήσετε μία αναζήτηση μπορείτε με τη χρήση της <b>μπάρας φιλτραρίσματος αποτελεσμάτων –η οποία βρίσκεται στο επάνω δεξί άκρο των αποτελεσμάτων αναζήτησης</b> – να περιορίσετε τα ήδη εμφανισμένα αποτελέσματα κατά το έτος ή τα έτη τα οποία επιθυμείτε.' +

        'Έστω λοιπόν  ότι πραγματοποιείτε αναζήτηση με τους όρους <b>«<i>αποδοχή</i>»</b>, <b>«<i>κληρονομίας</i>»</b> και από τα αποτελέσματα που σας επιστρέφονται θέλετε να δείτε μόνο τις αποφάσεις που σχετίζονται με συγκεκριμένο έτος, απλά επιλέγετε από τα ανάλογα <b>φίλτρα</b> το έτος ή τα έτη που σας ενδιαφέρουν και κατευθείαν περιορίζεται ο αριθμός των εμφανιζόμενων αποτελεσμάτων.' +

        '<br><br>' +

        '<span style="margin:0px auto;font-family:verdana;font-size:15px;font-weight:bold;display:table;">* * *</span>' +

        '<br>' +

        '<b>Ιστορικό αναζήτησης</b>' +
        '<br>' +
        'Χρησιμοποιώντας το <b>Ιστορικό Αναζήτησης</b> που βρίσκεται αριστερά της μπάρας αναζήτησης μπορείτε να ανατρέξετε σε παλαιότερες αναζητήσεις σας. Πατήστε το βελάκι του ιστορικού αναζήτησης, επιλέξτε από την ανεπτυγμένη λίστα τους όρους που επιθυμείτε και πραγματοποιήστε νέα αναζήτηση, στη θέση της προηγούμενης.' +

        '<br><br>' +

        '<span style="margin:0px auto;font-family:verdana;font-size:15px;font-weight:bold;display:table;">* * *</span>' +

        '<br>' +


        '<b>Προηγμένες αναζητήσεις με χρήση ειδικών χαρακτήρων</b>' +
        '<br>' +
        'Δεν είστε υποχρεωμένοι να χρησιμοποιείτε ειδικούς χαρακτήρες αξίζει όμως να αναφέρουμε μερικούς από αυτούς διότι με λίγη εξάσκηση και πειραματισμό μπορείτε απογειώσετε την αποτελεσματικότητα των αναζητήσεων που πραγματοποιείτε:' +

        '<br><br>' +

        '<b>Αναζήτηση εύρους</b>' +
        '<br>' +
        'Με την αναζήτηση <b>«εύρους»</b> δίνετε εντολή στη μηχανή αναζήτησης να εντοπίσει αποφάσεις οι οποίες περιέχουν τους όρους τους οποίους επιθυμείτε, μόνο εάν βρίσκονται σε συγκεκριμένη απόσταση μεταξύ τους.' +
        '<br>' +
        'Δηλώνεται με το <b>μαθηματικό σύμβολο του <b>«περίπου»</b> (~)</b>. Για να το γράψετε κρατάτε πατημένο το πλήκτρο SHIFT και στη συνέχεια πατάτε το πλήκτρο <b>(`)</b> –βρίσκεται αριστερά από το πλήκτρο 1 και ακριβώς πάνω από το πλήκτρο ΤΑΒ–<br>' +
        'Έστω λοιπόν ότι επιθυμείτε να αναζητήσετε αποφάσεις που περιέχουν τους όρους <b>«συμπληρωματική»</b>, <b>«ένορκη»</b>, <b>«δήλωση»</b>, <b>«αναγκαιότητα»</b> εντός μίας φράσης και όχι σκόρπιες μέσα στο κείμενο, γράφετε στη μπάρα αναζήτησης ως εξής:' +
        '<br>' +
        '<b>"συμπληρωματική ένορκη δήλωση αναγκαιότητα"~10</b>' +
        '<br>' +
        'Με τον παραπάνω συνδυασμό χαρακτήρων λέτε στη μηχανή αναζήτησης ότι επιθυμείτε να σας επιστρέψει αποτελέσματα που περιέχουν τους όρους που εισάγατε εντός το πολύ 10 λέξεων απόσταση μεταξύ τους, εάν επιθυμείτε μεγαλύτερο εύρος απόστασης απλά αλλάζετε τον αριθμό μετά το σύμβολο <b>«~»</b>. Οι όροι που επιθυμείτε να αναζητήσετε περικλείονται <b>πάντα</b> με εισαγωγικά και απέξω από αυτά –εκ δεξιών– αναγράφεται το σύμβολο του περίπου (~) κολλητά με τον αριθμό που επιθυμείτε.' +
        '<br>' +
        'Έτσι θα σας επιστραφούν αποτελέσματα όπως <b>«...συμπληρωματικής ένορκης δήλωσης του, την οποία επισυνάπτει. Για την αναγκαιότητα καταχώρησης της εν λόγω ένορκης δήλωσης...»</b> κ.λ.π.' +
        '<br>' +
        'Η αναζήτηση <b>«εύρους»</b> είναι είναι μία <b>πολύ χρήσιμη λειτουργία</b> ιδιαίτερα στην περίπτωση που <b>αναζητάτε νομικές φράσεις ή άρθρα νόμων</b>.<br>' +
        'Επί παραδείγματι, μπορείτε κάλλιστα να εντοπίσετε τις αποφάσεις ο οποίες αναφέρονται στο άρθρο 2 του νόμου 94(1)/1994 κάνοντας την εξής απλή αναζήτηση:' +
        '<br>' +
        '<b>"94(1)/1994 άρθρο 2"~10</b>' +

        '<br><br>' +

        '<b>Αστερίσκος (*)</b>. Για να το γράψετε κρατάτε πατημένο το πλήκτρο SHIFT και στη συνέχεια πατάτε το πλήκτρο 8:' +
        '<br>' +
        'Χρησιμοποιήστε τον πριν, μετά, αλλά και ενδιάμεσα σε ένα όρο και θα σας επιστραφούν αποτελέσματα με οποιοδήποτε συνδυασμό χαρακτήρων που ακολουθούν πλην του κενού.' +
        '<br>' +
        'Παραδείγματα:' +
        '<br>' +
        '<b>κτημα* > κτηματολόγιο, κτηματολογικά κ.λ.π.<br>*κτημα > απόκτημα, πλεονέκτημα κ.λ.π.<br>υπερ*β* > υπέρβαση, υπερέβαινε, υπερέβη</b>' +

        '<br><br>' +

        '<b>Το διαζευκτικό ή</b> το οποίο δηλώνεται με το αγγλικό OR (αναγράφεται πάντα με κεφαλαία):' +
        '<br>' +
        'Το χρησιμοποιείτε όταν θέλετε να αναζητήσετε αποφάσεις που περιέχουν τον ένα ή τον άλλον όρο.' +
        '<br>' +
        'Παράδειγμα:' +
        '<br>' +
        '<b>"άδειας διαίρεσης γης" OR "προσβολή πολεοδομικής άδειας"</b>' +

        '<br><br>' +

        'Το σύμβολο του <b>μείον (-)</b>:' +
        '<br>' +
        'Το χρησιμοποιείτε όταν θέλετε να αποκλείσετε, να εξαιρέσετε την αναζήτηση συγκεκριμένων όρων. Το εισάγετε ακριβώς πριν τον όρο τον οποίο επιθυμείτε να εξαιρεθεί από την αναζήτησή σας.<br>' +
        'Π.χ., γράφοντας: <b>νομιμοποίηση πολιτκής δίκης –ποινικής.</b>' +
        '<br>' +
        'Θα επιστραφούν αποφάσεις οι οποίες περιέχουν τους όρους <b>«νομιμοποίηση»</b>, <b>«πολιτκής»</b> και <b>«δίκης»</b> αλλά όχι αυτές οι οποίες περιέχουν τον όρο <b>«ποινικής»</b>.' +
        '</span>' +

        '<br><br><br>'
    }
    //TODO - add data, formulas and/or methods to support your view
});