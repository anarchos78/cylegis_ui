/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('cylegis.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },

    logOut: function () {
        //var redirectUrl = this.getViewModel().data.redirectUrl;
        var viewport = Ext.ComponentQuery.query('app-main')[0];
        var logoutUrl = cylegis.singleton.Utils.LogoutUrl;

        var logOutMask = new Ext.LoadMask({
            id: 'logout-mask',
            msg: "Παρακαλώ περιμένετε...",
            target: viewport
        });

        logOutMask.show();
        viewport.destroy();

        Ext.Function.defer(function () {
            window.location.replace(logoutUrl);
        }, 500);

        Ext.Function.defer(function () {
            logOutMask.hide();
        }, 3000);
    }
});
