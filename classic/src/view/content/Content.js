
Ext.define("cylegis.view.content.Content",{
    extend: "Ext.tab.Panel",
    xtype: 'contenttabpanel',
    id: 'content',

    requires: [
        "cylegis.view.content.ContentController",
        "cylegis.view.content.ContentModel",
        "cylegis.view.searchresult.SearchResult"
    ],

    controller: "content-content",
    viewModel: {
        type: "content-content"
    },

    margin: '10 0 0 0',
    border: true,
    flex: 2,
    activeTab: 0,
    defaults: {
        bodyStyle: {
            border: '0 0 0 0'
        }
    },

    items: [{
        xtype: 'recentapofaseis'
    }]
});
