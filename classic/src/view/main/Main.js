/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 */
Ext.define('cylegis.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    id: 'nv',

    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',

        'cylegis.view.main.MainController',
        'cylegis.view.main.MainModel',
        'cylegis.view.search.SearchForm',
        'cylegis.view.content.Content'
    ],

    controller: 'main',
    viewModel: 'main',

    ui: 'navigation',

    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,

    header: {
        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: '{logoName}'
            },
            flex: 0
        },
        titleAlign: 'center'
    },

    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        }
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    defaults: {
        bodyPadding: 10,
        tabConfig: {
            plugins: 'responsive',
            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left'
                },
                tall: {
                    iconAlign: 'top',
                    textAlign: 'center',
                    width: 120
                }
            }
        }
    },

    items: [{
        title: 'Αναζήτηση',
        iconCls: 'fa-search',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        // The following grid shares a store with the classic version's grid as well!
        items: [{
            xtype: 'searchform'
        }, {
            xtype: 'contenttabpanel'
        }]
    }, /*{
        title: 'Αποθηκευμένες<br>αναζητήσεις',
        iconCls: 'fa-database',
        bind: {
            html: '{loremIpsum}'
        }
    },*/ {
        title: 'Οδηγίες',
        iconCls: 'fa-info',
        scrollable: true,
        bodyPadding: 100,
        bodyStyle: {
            "background-color": "#fbfbfb"
        },
        bind: {
            html: '{instructions}'
        }
    }, {
        title: 'Έξοδος',
        iconCls: 'fa-sign-out',
        tabConfig: {
            textAlign: 'left',
            listeners: {
                click: 'logOut'
            }
        }
    }]
});
