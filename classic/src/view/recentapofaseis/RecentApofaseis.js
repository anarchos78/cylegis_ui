
Ext.define("cylegis.view.recentapofaseis.RecentApofaseis",{
    extend: "Ext.panel.Panel",
    id: 'recentapofaseis',
    xtype: 'recentapofaseis',

    requires: [
        "cylegis.view.recentapofaseis.RecentApofaseisController",
        "cylegis.view.recentapofaseis.RecentApofaseisModel",
        "cylegis.view.content.PdfViewer"
    ],

    controller: "recentapofaseis-recentapofaseis",
    viewModel: {
        type: "recentapofaseis-recentapofaseis"
    },

    bodyPadding: 20,
    autoScroll: true,

    title: 'Νέες Αποφάσεις',
    closable: false,
    tooltip: 'Νέες Αποφάσεις',

    items: [{
        xtype: 'dataview',
        margin: '20 80 20 80',
        store: Ext.create('cylegis.store.RecentApofaseis'),
        emptyText: 'Πρόβλημα κατά την φόρτωση των δεδομένων. Παρακάλω ανανεώστε την σελίδα!',
        loadMask: false,
        itemSelector: '.dikastirio-wrap',

        tpl: new Ext.XTemplate(
            '<span style="margin:0 auto;display:table;color:#3a3a3a;font-weight:bold;font-size:15px;text-decoration:underline;">' +
            'Αποφάσεις ' + cylegis.singleton.Utils.returncurrentmonth() + ' ' + cylegis.singleton.Utils.returncurrentyear() +
            '</span>' +
            '<br>' +
            '<tpl for=".">' +
                '<div class="dikastirio-wrap">' +

                    '<tpl if="dikastirio == \'aad_apofaseis_dikastikes\'">' +
                        '<span style="font-size:14px !important;font-weight:bold !important;">Αποφάσεις Ανωτάτου Δικαστηρίου Κύπρου</span><br><br>' +
                    '<tpl elseif="dikastirio == \'eparxiaka_astikes_apofaseis_dikastikes\'">' +
                        '<span style="font-size:14px !important;font-weight:bold !important;">Αποφάσεις Αστικές Επαρχιακών Δικαστηρίων</span><br><br>' +
                    '<tpl elseif="dikastirio == \'eparxiaka_enoikiaseon_apofaseis_dikastikes\'">' +
                        '<span style="font-size:14px !important;font-weight:bold !important;">Αποφάσεις Δικαστηρίων Ελέγχου Ενοικιάσεων</span><br><br>' +
                    '<tpl elseif="dikastirio == \'eparxiaka_ergatiko_apofaseis_dikastikes\'">' +
                        '<span style="font-size:14px !important;font-weight:bold !important;">Αποφάσεις Δικαστηρίων Εργατικών Διαφορών</span><br><br>' +
                    '<tpl elseif="dikastirio == \'eparxiaka_oikogeneiako_apofaseis_dikastikes\'">' +
                        '<span style="font-size:14px !important;font-weight:bold !important;">Αποφάσεις Οικογενειακών Δικαστηρίων</span><br><br>' +
                    '<tpl elseif="dikastirio == \'eparxiaka_poinikes_apofaseis_dikastikes\'">' +
                        '<span style="font-size:14px !important;font-weight:bold !important;">Αποφάσεις Ποινικές Επαρχιακών Δικαστηρίων</span><br><br>' +
                    '<tpl else>' +
                        '<span style="font-size:14px !important;font-weight:bold !important;">Αποφάσεις Διοικητικών Δικαστηρίων</span><br><br>' +
                    '</tpl>' +

                    '<tpl if="apofaseis.length">' +
                        '<tpl for="apofaseis">' +
                            '<span class="dikastirio" ' +
                            'style="cursor:pointer" ' +
                            'onmouseover="this.style.textDecoration=\'underline\';" ' +
                            'onmouseout="this.style.textDecoration=\'none\';" child:id="{ida}" child:model="{model}" child:type="{type}" child:title="{title}" child:case_num="{case_num}">' +
                            //'[{case_num}]&nbsp;&nbsp;{title}' +
                            '{title}' +
                            '</span><br><br>' +
                        '</tpl>' +
                    '<tpl else>' +
                        'Δεν υπάρχουν νέες αποφάσεις.<br><br>' +
                    '</tpl>' +

                '</div>' +
            '</tpl>'
        ),

        listeners: {
            itemclick: function(view, record, item, index, e, eOpts) {
                var child = e.getTarget('.dikastirio', 3, true);
                var ida = child && child.getAttribute('id', 'child');
                var model = child && child.getAttribute('model', 'child');
                var type = child && child.getAttribute('type', 'child');
                var title = child && child.getAttribute('title', 'child');
                var case_num = child && child.getAttribute('case_num', 'child');
                var kwlist = [];
                var contenttabpanel = Ext.ComponentQuery.query('#content')[0];
                var existingapofasitab = contenttabpanel.items.findBy(function (i) {
                    return i.id == model + '_' + ida;
                });

                switch (model) {
                    case 'aad_apofaseis_dikastikes':
                        var paneltitle = 'Ανώτατου - ';
                        break;
                    case 'eparxiaka_astikes_apofaseis_dikastikes':
                        paneltitle = 'Επ. Αστικές - ';
                        break;
                    case 'eparxiaka_enoikiaseon_apofaseis_dikastikes':
                        paneltitle = 'Επ. Ενοικιάσεων - ';
                        break;
                    case 'eparxiaka_ergatiko_apofaseis_dikastikes':
                        paneltitle = 'Επ. Εργατικό - ';
                        break;
                    case 'eparxiaka_oikogeneiako_apofaseis_dikastikes':
                        paneltitle = 'Επ. Οικογενειακό - ';
                        break;
                    case 'eparxiaka_poinikes_apofaseis_dikastikes':
                        paneltitle = 'Επ. Ποινικές - ';
                        break;
                    case 'dioikitika_apofaseis_dikastikes':
                        paneltitle = 'Διοικητικών - ';
                        break;
                }

                if (ida) {
                    if (!existingapofasitab) {
                        if (type != 'text') {
                            var user = Ext.util.Cookies.get('user');
                            if (user == null || user == undefined) {
                                Ext.Msg.show({
                                    title: 'Προσοχη!',
                                    msg: '<span style="display:block!important;white-space:nowrap!important;">' +
                                    'Δεν είστε συνδεδεμένοι.' +
                                    '<br>' +
                                    'Παρακάλω κάντε είσοδο στο λογαριασμό σας.' +
                                    '</span>',
                                    buttons: Ext.Msg.OK,
                                    closable: false,
                                    fn: function(buttonId) {
                                        if (buttonId === "ok") {
                                            window.location.replace(cylegis.singleton.Utils.LogoutUrl);
                                        }
                                    }
                                });
                            } else {
                                var url = cylegis.singleton.Utils.ApofasiUrl + model + '/' + ida + '/';
                                var apofasitabpdf = Ext.create('cylegis.view.content.PdfViewer', {
                                    id: model + '_' + ida,
                                    //title: paneltitle + case_num,
                                    title: paneltitle + cylegis.singleton.Utils.trimtitle(title),
                                    tooltip: title,
                                    downloadmodel: model,
                                    download_id: ida
                                });

                                apofasitabpdf.down('component').setSource(url);
                                contenttabpanel.add(apofasitabpdf);
                                contenttabpanel.setActiveTab(apofasitabpdf);
                            }
                        } else {
                            var apofasitab = Ext.create('cylegis.view.content.ApofasiTextPanel', {
                                id: model + '_' + ida,
                                //title: paneltitle + case_num,
                                title: paneltitle + cylegis.singleton.Utils.trimtitle(title),
                                tooltip: title,
                                downloadmodel: model,
                                download_id: ida
                            });

                            var dataview= apofasitab.down('dataview');
                            var dataviewstore = Ext.create('cylegis.store.Apofasi');
                            var dataviewstoreurl = cylegis.singleton.Utils.ApofasiUrl + model + '/' + ida + '/';
                            var cyclebtnid = apofasitab.down('toolbar').items.items[0].id;
                            var cyclebtn = Ext.ComponentQuery.query('#' + cyclebtnid)[0];

                            dataview.bindStore(dataviewstore);
                            dataviewstore.load({
                                scope: this,
                                url: dataviewstoreurl,
                                params: {
                                    kwlist: String(kwlist)
                                }
                            });

                            // Remove the text highlight button if no highlight terms have been found
                            if (!kwlist.length) {
                                cyclebtn.destroy();
                            }

                            contenttabpanel.add(apofasitab);
                            contenttabpanel.setActiveTab(apofasitab);
                        }
                    } else {
                        contenttabpanel.setActiveTab(existingapofasitab);
                    }
                }

            }
        }
    }]

});
