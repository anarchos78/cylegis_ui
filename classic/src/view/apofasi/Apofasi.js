Ext.define("cylegis.view.content.ApofasiTextPanel", {
    extend: 'Ext.panel.Panel',
    xtype: 'apoftextpanel',

    title: 'Hello',
    tooltip: 'Hello',
    closable: true,
    bodyBorder: true,
    bodyPadding: 20,
    autoScroll: true,

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        style: {
            backgroundColor: '#f6f6f6'
        },
        items: [{
            xtype: 'cycle',
            prependText: 'Υπογράμμιση: ',
            showText: true,
            scope: this,
            action: 'togglehighlight',
            style: {
                borderColor: 'transparent'
            },
            menu: {
                items: [{
                    text: 'Ενεργή',
                    checked: true,
                    glyph: 'xf0c8@FontAwesome'
                }, {
                    text: 'Ανενεργή',
                    glyph: 'xf096@FontAwesome'
                }]
            },
            changeHandler: function (cycleBtn, activeItem) {
                var dataView = cycleBtn.up('panel').down('dataview');
                var dataviewhtml = dataView.el.dom.innerHTML;

                switch (activeItem.text) {
                    case 'Ενεργή':
                        dataviewhtml = dataviewhtml.replace(/nndd/gi, 'nnee');
                        //dataView.update('<span class=\"con\" style=\"font-size:15px;line-height:17px;font-family:Georgia, Times New Roman, Times, serif;\">' +
                        //    dataviewhtml +
                        //    '<\/span>'
                        //
                        dataView.update(dataviewhtml);
                        break;

                    case 'Ανενεργή':
                        dataviewhtml = dataviewhtml.replace(/nnee/gi, 'nndd');
                        //dataView.update('<span class=\"con\" style=\"font-size:15px;line-height:17px;font-family:Georgia, Times New Roman, Times, serif;\">' +
                        //    dataviewhtml +
                        //    '<\/span>'
                        //);
                        dataView.update(dataviewhtml);
                        break;
                }
            }
        }, '->', {
            xtype: 'button',
            text: 'Αποθήκευση',
            glyph: 'xf0c7@FontAwesome',
            menu: [{
                text: 'Αποθήκευση ως word doc',
                listeners: {
                    click: {
                        fn: function (menu, item, e, eOpts) {
                            var apoftextpanel = menu.up('menu').up('panel');
                            var viewport = Ext.ComponentQuery.query('app-main')[0];

                            //apply loading mask-msg
                            viewport.setLoading(
                                'Παρακαλώ περιμένετε...'
                                //new Ext.LoadMask({
                                //    target: viewport,
                                //    msg: '<b>' + 'Παρακαλώ περιμένετε...' + '</b>'
                                //})
                            );

                            var download_url = cylegis.singleton.Utils.ApofasiUrl + apoftextpanel.downloadmodel + '/' + apoftextpanel.download_id + '/docx/';
                            cylegis.singleton.Utils.downloadcookie(download_url);
                        }
                    }
                }
            }, {
                text: 'Αποθήκευση ως PDF',
                listeners: {
                    click: {
                        fn: function (menu, item, e, eOpts) {
                            var apoftextpanel = menu.up('menu').up('panel');
                            var viewport = Ext.ComponentQuery.query('app-main')[0];

                            //apply loading mask-msg
                            viewport.setLoading(
                                'Παρακαλώ περιμένετε...'
                                //new Ext.LoadMask({
                                //    target: viewport,
                                //    msg: '<b>' + 'Παρακαλώ περιμένετε...' + '</b>'
                                //})
                            );

                            var download_url = cylegis.singleton.Utils.ApofasiUrl + apoftextpanel.downloadmodel + '/' + apoftextpanel.download_id + '/pdf/';
                            cylegis.singleton.Utils.downloadcookie(download_url);
                        }
                    }
                }
            }]
        }]
    }],
    items: [{
        xtype: 'dataview',
        margin: '20 80 20 80',
        emptyText: 'Πρόβλημα κατά την φόρτωση των δεδομένων. Παρακάλω ανανεώστε την σελίδα!',
        loadMask: false,
        itemSelector: 'span.content-text',
        tpl: [
            //'<tpl for=".">' +
            //'<span style="font-size:15px;line-height:17px;font-family:Georgia, Times New Roman, Times, serif;">' +
            //'<b>{apofasi_title}</b>' +
            //    '<tpl if="apofasi_meros">' +
            //        '&nbsp;&nbsp;[ΜΕΡΟΣ {apofasi_meros}]' +
            //    '</tpl>' +
            //'<br/><br/>' +
            //'{case_num}<br/><br/>' +
            //'{apofasi_body}' +
            //'</span>' +
            //'</tpl>'
            '<tpl for=".">' +
                '<span style="font-size:15px;line-height:17px;font-family:Georgia, Times New Roman, Times, serif;">' +
                '{apofasi_body}' +
                '</span>' +
            '</tpl>'
        ]
    }]

});
