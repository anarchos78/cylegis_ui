Ext.define("cylegis.view.content.PdfViewer", {
    extend: "Ext.panel.Panel",
    xtype: 'pdfviewer',

    title: 'My PDF',
    closable: true,

    items: {
        xtype: 'component',
        autoEl: {
            tag: 'iframe',
            style: 'height: 100%; width: 100%; border: none',
            src: ''
        },

        setSource: function (src) {
            if (this.rendered) {
                this.autoEl.src = src;
            } else {
                this.autoEl.src = src;
            }
        }
    }

});