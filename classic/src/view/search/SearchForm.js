Ext.define("cylegis.view.search.SearchForm", {
    extend: "Ext.form.Panel",

    requires: [
        "cylegis.view.search.SearchFormController",
        "cylegis.view.search.SearchFormModel"
    ],

    xtype: "searchform",
    id: 'searchform',

    controller: "search-searchform",
    viewModel: {
        type: "search-searchform"
    },

    width: '100%',
    border: true,

    // The form will submit an AJAX request to this URL when submitted
    url: '', // TODO

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    listeners: {
        afterrender: function() {
            var searchbox = Ext.ComponentQuery.query('#searchbox')[0];
            var searchboxheight = searchbox.getHeight();

            this.setHeight(searchboxheight + 22);
        }
    },

    // The fields
    items: [{
        xtype: 'container',
        margin: '10 10 0 0',
        //border: 1,
        //style: {borderColor:'#000000', borderStyle:'solid', borderWidth:'1px'},
        layout: {
            type: 'vbox',
            align: 'end'
        },
        flex: 6,
        items: [{
            xtype: 'container',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            width: 200,
            items: [{
                xtype: 'combo',
                id: 'storedqueries',
                //minWidth: 200,
                emptyText: 'Ιστορικό Αναζήτησης',
                store: Ext.create('cylegis.store.StoredQueries'),
                minChars: 2,
                queryMode: 'local',
                typeAhead: true,
                displayField: 'trimedterm',
                valueField: 'term',
                multiSelect: false,
                fieldStyle: {
                    color: '#1a1a1a'
                },
                listConfig: {
                    getInnerTpl: function() {
                        //return '<div class=\'storedqueries-combo-item\' data-qtip=\'{term}\'>' + '<span>&bull;{trimedterm}</span>' + '</div>';
                        return '<div data-qtip=\'{term}\'>' + '<span>&bull;{trimedterm}</span>' + '</div>';
                    }
                    //cls: 'storedqueries-combo-row'
                },
                listeners: {
                    select: {
                        scope: this,
                        fn: function(record, eOpts) {
                            var value = record.value;
                            var clearbtn = Ext.ComponentQuery.query('#clear-btn')[0];
                            var searchbox = Ext.ComponentQuery.query('#searchbox')[0];
                            var storedqueries = Ext.ComponentQuery.query('#storedqueries')[0];

                            clearbtn.btnEl.dom.click();
                            searchbox.setValue(value);
                            searchbox.focus();
                            storedqueries.clearValue();
                        }
                    }
                }
            }]
        }, {
            xtype: 'container',
            layout: {
                type: 'vbox',
                align: 'end'
            },
            items: [{
                xtype: 'checkboxfield',
                id: 'keepfilters',
                boxLabel: 'Διατήρηση φίλτρων',
                //boxLabelAlign: 'before', // Placing the "label text" on the left ('before') breaks the layout!!!
                name: 'keepfilters',
                inputValue: '1',
                listeners: {
                    change: {
                        fn: function(field, newValue, oldValue, eOpts) {
                            if (field.getRawValue() == false) {
                                var clearbtn = Ext.ComponentQuery.query('#clear-btn')[0];
                                clearbtn.btnEl.dom.click();
                            }
                        }
                    },
                    focus: {
                        fn: function() {
                            var advsearchbtn = Ext.ComponentQuery.query('#adv-search-btn')[0];
                            var form = Ext.ComponentQuery.query('#searchform')[0];
                            var formheight = form.getHeight();

                            if (formheight <= 52) {
                                advsearchbtn.btnEl.dom.click();
                            }
                        }
                    }
                }
            }]
        }]
    }, {
        xtype: 'container',
        margin: '10 10 0 0',
        flex: 5,
        items: [{
            xtype: 'textfield',
            margin: '0 0 10 0',
            id: 'searchbox',
            width: '100%',
            emptyText: 'Παρακαλώ βάλτε όρο αναζήτησης',
            emptyCls: 'empty-search',
            fieldStyle: {
                color: '#1a1a1a'
            },
            name: 'qs',
            listeners: {
                specialkey: 'onEnterDown'
            }
        }, {
            xtype: 'container',
            margin: '0 0 10 0',
            layout: {
                type: 'hbox',
                vertical: false,
                align: 'top'
            },
            items: [{
                xtype: 'combo',
                margin: '0 10 0 0',
                id: 'model',
                editable: false,
                displayField: 'displayvalue',
                valueField: 'model',
                queryMode: 'local',
                emptyText: 'Αποφάσεις',
                multiSelect: true,
                fieldStyle: {
                    color: '#1a1a1a'
                },
                flex: 2,

                store: {
                    fields: ['model', 'displayvalue'],

                    data: [{
                        model: 'aad_apofaseis_dikastikes',
                        displayvalue: 'Ανώτατου'
                    }, {
                        model: 'eparxiaka_astikes_apofaseis_dikastikes',
                        displayvalue: 'Επ. Αστικές'
                    }, {
                        model: 'eparxiaka_enoikiaseon_apofaseis_dikastikes',
                        displayvalue: 'Επ. Ενοικιάσεων'
                    }, {
                        model: 'eparxiaka_ergatiko_apofaseis_dikastikes',
                        displayvalue: 'Επ. Εργατικό'
                    }, {
                        model: 'eparxiaka_oikogeneiako_apofaseis_dikastikes',
                        displayvalue: 'Επ. Οικογενειακό'
                    }, {
                        model: 'eparxiaka_poinikes_apofaseis_dikastikes',
                        displayvalue: 'Επ. Ποινικές'
                    }, {
                        model: 'dioikitika_apofaseis_dikastikes',
                        displayvalue: 'Διοικητικών'
                    }]
                }
                //listeners: {
                //    select: {
                //        scope: this,
                //        fn: function(record, eOpts) {
                //            var values = record.value;
                //            var apofasi_meros = Ext.ComponentQuery.query('#apofasi_meros')[0];
                //            var apofasi_num = Ext.ComponentQuery.query('#apofasi_num')[0];
                //
                //            if (Ext.Array.indexOf(values, 'aad_apofaseis_dikastikes') > -1) {
                //                apofasi_meros.setDisabled(false);
                //                apofasi_num.setDisabled(false);
                //            } else {
                //                apofasi_meros.clearValue();
                //                apofasi_meros.setDisabled(true);
                //
                //                apofasi_num.setValue('');
                //                apofasi_num.setDisabled(true);
                //            }
                //        }
                //    }
                //}
            }, {
                xtype: 'combo',
                margin: '0 10 0 0',
                id: 'apofasi_meros',
                //disabled: true,
                editable: false,
                displayField: 'displayvalue',
                valueField: 'apofasi_meros',
                queryMode: 'local',
                emptyText: 'Μέρος',
                multiSelect: true,
                fieldStyle: {
                    color: '#1a1a1a'
                },
                flex: 2,

                store: {
                    fields: ['apofasi_meros', 'displayvalue'],

                    data: [{
                        apofasi_meros: 1,
                        displayvalue: 'Μέρος 1'
                    }, {
                        apofasi_meros: 2,
                        displayvalue: 'Μέρος 2'
                    }, {
                        apofasi_meros: 3,
                        displayvalue: 'Μέρος 3'
                    }, {
                        apofasi_meros: 4,
                        displayvalue: 'Μέρος 4'
                    }]
                }
            }, {
                xtype: 'textfield',
                id: 'apofasi_num',
                //disabled: true,
                emptyText: 'Αρ. Απόφασης [π.χ 606/2009]',
                //minLength: 4,
                //maxLength: 4,
                fieldStyle: {
                    color: '#1a1a1a'
                },
                // Remove spinner buttons, and arrow key and mouse wheel listeners
                hideTrigger: true,
                keyNavEnabled: false,
                mouseWheelEnabled: false,
                flex: 2
            }]
        }, {
            xtype: 'container',
            layout: {
                type: 'hbox',
                vertical: false,
                align: 'top'
            },
            items: [{
                xtype: 'combo',
                margin: '0 10 0 0',
                id: 'apofasi_month',
                editable: false,
                displayField: 'displayvalue',
                valueField: 'apofasi_month',
                queryMode: 'local',
                emptyText: 'Μήνας',
                multiSelect: true,
                fieldStyle: {
                    color: '#1a1a1a'
                },
                flex: 2,

                store: {
                    fields: ['apofasi_month', 'displayvalue'],

                    data: [{
                        apofasi_month: 1,
                        displayvalue: '1'
                    }, {
                        apofasi_month: 2,
                        displayvalue: '2'
                    }, {
                        apofasi_month: 3,
                        displayvalue: '3'
                    }, {
                        apofasi_month: 4,
                        displayvalue: '4'
                    }, {
                        apofasi_month: 5,
                        displayvalue: '5'
                    }, {
                        apofasi_month: 6,
                        displayvalue: '6'
                    }, {
                        apofasi_month: 7,
                        displayvalue: '7'
                    }, {
                        apofasi_month: 8,
                        displayvalue: '8'
                    }, {
                        apofasi_month: 9,
                        displayvalue: '9'
                    }, {
                        apofasi_month: 10,
                        displayvalue: '10'
                    }, {
                        apofasi_month: 11,
                        displayvalue: '11'
                    }, {
                        apofasi_month: 12,
                        displayvalue: '12'
                    }]
                }
            }, {
                xtype: 'textfield',
                margin: '0 10 0 0',
                id: 'apofasi_case_num',
                emptyText: 'Αρ. Υπόθεσης [π.χ 167/2010]',
                //minLength: 4,
                //maxLength: 4,
                fieldStyle: {
                    color: '#1a1a1a'
                },
                // Remove spinner buttons, and arrow key and mouse wheel listeners
                hideTrigger: true,
                keyNavEnabled: false,
                mouseWheelEnabled: false,
                flex: 2
            }, {
                xtype: 'textfield',
                id: 'apofasi_year',
                emptyText: 'Έτος [π.χ 2000]',
                //minLength: 4,
                //maxLength: 4,
                fieldStyle: {
                    color: '#1a1a1a'
                },
                // Remove spinner buttons, and arrow key and mouse wheel listeners
                hideTrigger: true,
                keyNavEnabled: false,
                mouseWheelEnabled: false,
                flex: 2,
                regex: /(^([1-2]\d{3})(-[1-2]\d{3})$|^([1-2]\d{3})(,\s*[1-2]\d{3})*$)/,
                invalidText: 'Μη έγκυρα στοιχεία!<br><br>' +
                'Παραδείγματα:<br>' +
                'Για εύρος ετών: 2012-2015<br>' +
                'Για μεμονωμένα έτη: 2000,2005,2015<br>'

            }]
        }]
    }, {
        xtype: 'container',
        margin: '10 10 0 0',
        layout: {
            type: 'vbox',
            align: 'end'
        },
        items: [{
            xtype: 'container',
            id: 'searchbuttons',
            flex: 0,
            items: [{
                xtype: 'button',
                id: 'search-btn',
                glyph: 'xf002@FontAwesome',
                margin: '0 10 0 0',
                tooltip: 'Αναζήτηση',
                listeners: {
                    click: 'onSearchButtonClick'
                }
            }, {
                xtype: 'button',
                id: 'adv-search-btn',
                enableToggle: true,
                pressed: false,
                glyph: 'xf067@FontAwesome',
                margin: '0 0 0 0',
                tooltip: 'Σύνθετη αναζήτηση',
                listeners: {
                    toggle: 'onAdvanceSearchButtonToggle'
                }
            }]
        }, {
            // This behaves as a spacer [aligns the fields]
            xtype: 'container',
            items: [{
                xtype: 'button',
                id: 'clear-btn',
                glyph: 'xf00d@FontAwesome',
                margin: '10 0 0 00',
                tooltip: 'Καθαρισμός φίλτρων',
                handler: function() {
                    var keepfilterscheckbox = Ext.ComponentQuery.query('#keepfilters')[0];
                    var modelcombo = Ext.ComponentQuery.query('#model')[0];
                    var apofasi_meroscombo = Ext.ComponentQuery.query('#apofasi_meros')[0];
                    var apofasi_num_field = Ext.ComponentQuery.query('#apofasi_num')[0];
                    var apofasi_monthcombo = Ext.ComponentQuery.query('#apofasi_month')[0];
                    var apofasi_case_field = Ext.ComponentQuery.query('#apofasi_case_num')[0];
                    var apofasi_yearfield = Ext.ComponentQuery.query('#apofasi_year')[0];

                    keepfilterscheckbox.setValue(false);
                    modelcombo.clearValue();
                    apofasi_meroscombo.clearValue();
                    apofasi_num_field.setValue('');
                    apofasi_monthcombo.clearValue();
                    apofasi_yearfield.setValue('');
                    apofasi_case_field.setValue('');
                }
            }],
            listeners: {
                afterrender: function() {
                    var advsearchbar = Ext.ComponentQuery.query('#searchbuttons')[0];
                    var advsearchbarwidth = advsearchbar.getWidth();

                    this.setWidth(advsearchbarwidth);
                }
            }
        }]
    }]
});