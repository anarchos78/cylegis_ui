
Ext.define("cylegis.view.searchresult.SearchResult",{
    extend: "Ext.grid.Panel",
    xtype: 'searchresult',
    id: 'searchresult',

    requires: [
        "cylegis.view.searchresult.SearchResultController",
        "cylegis.view.searchresult.SearchResultModel",
        "cylegis.store.SearchResult",
        "cylegis.view.searchresult.PreviewPluginOverride"
    ],

    store: Ext.create('cylegis.store.SearchResult'),
    //store: Ext.data.StoreManager.lookup('searchresultstore'),

    controller: "searchresult-searchresult",
    viewModel: {
        type: "searchresult-searchresult"
    },

    closable: true,
    bodyBorder: true,
    allowDeselect: true,

    dockedItems: [{
        xtype: 'toolbar',
        id: 'searchresulttoolbar',
        margin: '0 0 -18 0', // TODO I had to set negative margin becaise when I set the panel "closable: true" a bottom [to toolbar] margin appears
        dock: 'top',
        style: {
            backgroundColor: '#f6f6f6'
        },
        items: [{
            xtype: 'button',
            disabled: true,
            //border: false,
            style: {
                borderColor: 'transparent'
            },
            id: 'apofaseiscounttext'
        }, '->']
    }],

    title: '<span style="color:#bf1111;font-weight:bold;">Απ. Αναζήτησης</span>',
    tooltip: 'Απ. Αναζήτησης',

    hideHeaders: true,

    viewConfig: {
        deferEmptyText: true,
        emptyText: '<span style=\"' +
                   'margin:0px auto;' +
                   'padding-top:25px;' +
                   'width:500px;' +
                   'height:80px;' +
                   'color:#df1717 !important;' +
                   'font-weight:bold;' +
                   'position:absolute;' +
                   'top:35%;' +
                   'left:50%;' +
                   'margin-left:-250px;' +
                   'margin-top:-40px;' +
                   'text-align:center;' +
                   'border:solid 1px #d7d7d7;' +
                   'background-color: #e7e7e7 !important;' +
                   '-moz-border-radius:20px;' +
                   '-webkit-border-radius:20px;' +
                   '-khtml-border-radius:20px;' +
                   'border-radius:20px;' +
                   '-moz-box-shadow:5px 5px 5px #dbd1d1;' +
                   '-webkit-box-shadow:5px 5px 5px #dbd1d1;' +
                   'box-shadow:5px 5px 10px #dbd1d1;' +
                   '\">' +
                   'Ο παραπάνω όρος αναζήτησης δεν επιστρέφει αποτελέσματα!' +
                   '<br/>' +
                   'Παρακαλώ επιλέξτε διαφορετικό όρο.' +
                   '</span>',
        plugins: [{
            ptype: 'previewoverride',
            bodyField: 'content',
            expanded: true
        }],
        loadingText: 'Παρακαλώ περιμένετε...'
    },

    columns: [
        { text: 'apofasi_month', dataIndex: 'apofasi_month', hidden: true },
        { text: 'apofasi_year', dataIndex: 'apofasi_year', hidden: true },
        { text: 'doc_id', dataIndex: 'doc_id', hidden: true },
        { text: 'grid_title', dataIndex: 'grid_title', hidden: true },
        { text: 'ida', dataIndex: 'ida', hidden: true },
        { text: 'kwlist', dataIndex: 'kwlist', hidden: true },
        { text: 'model', dataIndex: 'model', hidden: true },
        { text: 'type', dataIndex: 'type', hidden: true },
        {
            text: 'head',
            xtype: 'templatecolumn',
            tpl: new Ext.XTemplate(
                '<tpl for=".">' +
                    '<tpl if="model == \'aad_apofaseis_dikastikes\'">' +
                        '<b>{title}</b>&nbsp;&nbsp;<b>(Απόφαση Ανωτάτου Δικαστηρίου Κύπρου)</b><br>' +
                        '<tpl if="apofasi_meros">' +
                            '<b>ΜΕΡΟΣ {apofasi_meros}</b><br>' +
                        '</tpl>' +
                        'Αρ. Υπόθεσης: {case_num}' +
                    '<tpl elseif="model == \'eparxiaka_astikes_apofaseis_dikastikes\'">' +
                        '<b>{title}</b>&nbsp;&nbsp;<b>(Απόφαση Αστικη Επαρχιακών Δικαστηρίων)</b><br>' +
                        'Αρ. Αγωγής: {case_num}' +
                    '<tpl elseif="model == \'eparxiaka_enoikiaseon_apofaseis_dikastikes\'">' +
                        '<b>{title}</b>&nbsp;&nbsp;<b>(Απόφαση Δικαστηρίων Ελέγχου Ενοικιάσεων)</b><br>' +
                        'Αρ. Αγωγής: {case_num}' +
                    '<tpl elseif="model == \'eparxiaka_ergatiko_apofaseis_dikastikes\'">' +
                        '<b>{title}</b>&nbsp;&nbsp;<b>(Απόφαση Δικαστηρίων Εργατικών Διαφορών)</b><br>' +
                        'Αρ. Αγωγής: {case_num}' +
                    '<tpl elseif="model == \'eparxiaka_oikogeneiako_apofaseis_dikastikes\'">' +
                        '<b>{title}</b>&nbsp;&nbsp;<b>(Απόφαση Οικογενειακών Δικαστηρίων)</b><br>' +
                        'Αρ. Αγωγής: {case_num}' +
                    '<tpl elseif="model == \'eparxiaka_poinikes_apofaseis_dikastikes\'">' +
                        '<b>{title}</b>&nbsp;&nbsp;<b>(Απόφαση Ποινική Επαρχιακών Δικαστηρίων)</b><br>' +
                        'Αρ. Αγωγής: {case_num}' +
                    '<tpl else>' +
                        '<b>{title}</b>&nbsp;&nbsp;<b>(Απόφαση Διοικητικών Δικαστηρίων)</b><br>' +
                        'Αρ. Αγωγής: {case_num}' +
                    '</tpl>' +
                '</tpl>'
            ),
            flex: 1
        }
    ],

    listeners: {
        select: 'onRowSelect'
    }
});
